import 'dotenv/config';
import cors from 'cors';
import express from 'express';
import uuidv4 from 'uuid/v4';

const app = express();

app.use(cors());

let users = {
    1: {
      id: '1',
      username: 'Robin Wieruch',
    },
    2: {
      id: '2',
      username: 'Dave Davids',
    },
  };
  
  let messages = {
    1: {
      id: '1',
      text: 'Hello World',
      userId: '1',
    },
    2: {
      id: '2',
      text: 'By World',
      userId: '2',
    },
  };

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/', (req, res) => {
    return res.send('Received a GET HTTP method');
});
  
app.post('/', (req, res) => {
    return res.send('Received a POST HTTP method');
});
  
app.put('/', (req, res) => {
    return res.send('Received a PUT HTTP method');
});

app.delete('/', (req, res) => {
    return res.send('Received a DELETE HTTP method');
});

app.get('/users', (req, res) => {
    return res.send(Object.values(users));
});
  
app.get('/users/:userId', (req, res) => {
    return res.send(users[req.params.userId]);
});

app.post('/messages', (req, res) => {
    const id = uuidv4();
    const message = {
      id,
    };
  
    messages[id] = message;
  
    return res.send(message);
});

app.listen(process.env.PORT, () =>
    console.log(`Example app listening on port ${process.env.PORT}!`),
);

//app.listen(3000, () =>
    //console.log('Example app listening on port 3000! '),
    //console.log('Hello Node.js project. '),
    //);
    //console.log(process.env.MY_SECRET),

//app.listen(process.env.PORT, () =>
//  console.log(`Example app listening on port ${process.env.PORT}!`),
//);