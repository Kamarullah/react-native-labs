# react-native-labs

React Native Project (Hello World)

Javascript Engine
- NodeJS

Package Manager
- Chocolately

Stack Development
- Front End > React Native
- Backend > Express
- Database > MongoDB

How to install
- Install dependency, type command on cmd or terminal :
    npm install

- Running application, type command on cmd or terminal :
    npm start

Open On browser
- Enter url : http://localhost:3000

Regards,
A. Wahid Kamarullah

Update : 2019/04/08 20:51